from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('files/process', views.process, name='process'),
    path('files/detail/<str:value1>/<str:value2>', views.detail, name='detail'),
]
