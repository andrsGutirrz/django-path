

var tables = (function ($) {
    var init = function () {
        $("table").DataTable({
            dom: 'Bfrtip',
            buttons: [
            'copy', 'csv', 'excel','pdf'
            ],
            responsive:     true,
            deferRender:    true,
            scroller:       true,
            select:         true,
            pageLength:     10,
            language: {
                searchPlaceholder: "Search"
            }
        });
    };

    var eventHandlers = function () {

    };
    return {
        init: init,
        eventHandlers: eventHandlers
    };

})(jQuery);
$(document).ready(function() {
  tables.init();
  tables.eventHandlers();
});