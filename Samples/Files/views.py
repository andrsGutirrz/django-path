from django.shortcuts import render
from .logic import *

# Create your views here.
def index(request):
    context = {
        "webTitle" : "Files",
    }
    return render(request,"Files/index.html",context)

def process(request):
    #if post request came
    if request.method == 'POST':
        try:
            #getting values from post
            file1 = request.POST.get('file1')
            file2 = request.POST.get('file2')

            """ SOME LOGIC HERE Business layer, call some function 
                report(file1,file2) does a process in which extract dataframes from h5 files
                and push it into a list
            """
            htmlReport = processFiles(file1,file2)

            """ retrieve objects to template(html) """
            context = {
            "webTitle" : "Processed Files",
            "file1" : file1,
            "file2" : file2,
            "htmlReport" : htmlReport,
            }

            return render(request,"Files/process.html",context)
        except Exception as e :
            context = {
            "webTitle" : "Processed Files",
            "file1" : file1,
            "file2" : file2,
            "error" : "Error reading the files",
            "tb" : e,
            }
            return render(request, "Files/error.html", context)


def detail(request, value1,value2):
    """
    :param value1: key of dataframe 1
    :param value2: key of dataframe 2
    :return listHtml: DF resulting of comparing DF1 & DF2
        It receives 2 parameters, which are the keys for two DF
        in order to compare them
    """
    """logic"""
    try:
        #result =
        context = {
            "webTitle": "Detail",
            "value1": value1,
            "value2": value2,
            "htmlReport": "",
        }
        return render(request, 'Files/detail.html', context)
    except Exception as e:
        context = {
            "webTitle": "Detail",
            "value1": value1,
            "value2": value2,
            "error": "Error processing details",
            "tb": e,
        }
        return render(request, 'Files/error.html', context)