import numpy as np
import pandas as pd
from pandas import HDFStore
import h5py

"""
    listHtml will storage all dataframes in html format
"""
listHtml = []

def processFiles(file1,file2):
    """
    :param file1: Path of file1
    :param file2: Path of file2
    :return: list of dataframes in html format

    It takes tha path of 2 h5 files :file1 and :file2

    It performs the logic that is needed
    Its important to add the dataframes into the listHtml
    in html format using dataframe.to_html() function

    """

    """The following code is just PoC, need to be replaced by your logic"""
    listHtml.clear() # remove all elements
    df1 = pd.read_hdf(file1, 'df')
    df2 = pd.read_hdf(file2, 'df')
    listHtml.append(df1.to_html(classes="table table-striped",escape=False)) # uses classes from Bootstrap framework
    listHtml.append(df2.to_html(classes="table table-striped",escape=False))

    """ Its IMPORTANT to return a list of dataframes in html format! mandatory!"""
    return listHtml

"""
    #######################################################################
    UTILS & PoC
    The following code is used for creating dummy h5 files to test
    #######################################################################
"""
def creatingFile():
    nparray = np.arange(100,200).reshape(10, 10)

    filepath = 'C:\\Users\\asge\\Desktop\\dset2.h5'

    file = h5py.File(filepath, 'w')  # w stands for writing
    dataset = file.create_dataset("df", data=nparray)

    print("Dataset dataspace is", dataset.shape)
    print("Dataset Numpy datatype is", dataset.dtype)
    print("Dataset name is", dataset.name)
    print("Dataset is a member of the group", dataset.parent)
    print("Dataset was created in the file", dataset.file)
    #
    # Close the file before exiting
    #
    file.close()

def readingFilePath(filepath):
    file = h5py.File(filepath, 'r')  # r stands for reading
    #print(file.keys())  # prints the list of data we save into ahore file .h5
    newnparray = file.get('df')
    newnparray = np.array(newnparray)
    print(newnparray)
    file.close()
    return newnparray

def creatingFileDF():
    df1 = {
        'name': ["annie", "bob", "charlie", "damon", "erick", "fry", "goldman"],
        'age': [20, 27, 35, 55, 18, 21, 35],
        'designation': ["VP", "CEO", "CFO", "VP", "VP", "CEO", "MD"],
        'link': ["<a href=\"/files/detail/20/40\">Detail</a>" ] * 7
    }
    df2 = {
        'name': ["goo", "foo", "moo", "poo", "loo", "fley", "Guti"],
        'age': [20, 27, 35, 55, 18, 21, 35],
        'designation': ["VP", "CEO", "CFO", "VP", "VP", "CEO", "MD"],
        'link': ["<a href=\"/files/detail/20/40\">Detail</a>"] * 7
    }

    pandas_dataframe1 = pd.DataFrame(df1)
    pandas_dataframe2 = pd.DataFrame(df2)

    filepath1 = 'C:\\Users\\asge\\Desktop\\dset3.h5'
    filepath2 = 'C:\\Users\\asge\\Desktop\\dset4.h5'

    pandas_dataframe1.to_hdf(filepath1, key='df')
    pandas_dataframe2.to_hdf(filepath2, key='df')


def readingFilePathDF(filepath):
    df1 = pd.read_hdf(filepath, 'df')
    return df1.to_html()

#creatingFile()
#readingFilePath('C:\\Users\\asge\\Desktop\\dset2.h5')
#creatingFileDF()
#print(readingFilePathDF('C:\\Users\\asge\\Desktop\\dset3.h5'))

if  __name__ == '__main__':
    creatingFileDF()
    print('New Files generated!')