# Django Path

# Web application that renders h5 files (.h5)

1. ## Introduce the path of two h5 files, and then click on process button  

   <img src="/uploads/70029c17dcadc4147eb9a1bfed48a8fc/image.png" width="600" />
    
2. ## View your results in html table format  

    <img src="/uploads/6379d1a6a0b7775ab0314f1f224e31d8/image.png" width="600" />
    
3. ## View details for each row you are interested 

    <img src="/uploads/cfdfab5aeaad94bb75dbf63157da9d07/image.png" width="500" />


<hr>
<hr>

### Read the wiki for more information about usage